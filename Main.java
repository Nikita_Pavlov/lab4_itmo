import Classes.*;
import Classes.Action;
import Enums.*;
import Interfaces.Changeable;
import MyOutput.Stdout;

public class Main {
    public static void main(String[] args) {
        /*
        Малыш резким движением вырвался из рук Бетан; лицо его было мокрым от слез. //act0
        Малыш побежал в свою комнату и бросился на кровать. //act1
        Боссе и Бетан кинулись вслед за ним. //act2
        Прибежала и мама. //act3
        Но Малыш не обращал на них никакого внимания -- он весь трясся от плача. //act4
        Теперь день рождения был испорчен. //act5
        Малыш решил быть целый день веселым, даже если ему и не подарят собаку. //act6
        Но получить в подарок плюшевого щенка -- это уж слишком! //act7
        Когда он об этом вспоминал, его плач превращался в настоящий стон, //act8
        и он все глубже зарывался головой в подушку.
        Мама, Боссе и Бетан стояли вокруг кровати.//act9
        Всем им было тоже очень грустно.//act10
         */

        Stdout outer = new Stdout();

        Actor mal = new Actor("Малыш", Gender.MALE);
        Actor betun = new Actor("Бетан", Gender.MALE);
        //Actor mal = new Actor("Малыш", Gender.NONE);

        //Малыш резким движением вырвался из рук Бетан; лицо его было мокрым от слез. //act0

        ActionVerb vrvlsya = new ActionVerb("вырвал", VerbType.REFLECTIVE);
        Action act0 = new Action(mal,Direction.ABRPT_MOVE,vrvlsya,Direction.FROM_HANDS,betun,Case.GENITIVE);
        Changeable his = new Changeable(){
            Gender gender;
            public String name;
            @Override
            public void change(Gender gender) {
                String[] names = {"его","ее","их","его"};
                this.name = names[gender.ordinal()];
            }
            @Override
            public void change(Case g_case) {
            }
            @Override
            public String toString(){
                return this.name;
            }
        };
        //his.change(Gender.MALE);
        Actor face = new Actor ("лицо", Gender.MALE);
        act0.addAction(Conjunction.COMMA_DOT,face,his,Direction.WAS_WET);
        outer.println(act0.doAction());

        Secondary room = new Secondary(ObjectType.ROOM, Gender.FEMALE);
        ActionVerb poran = new ActionVerb("побежал", VerbType.SIMPLE);
        Action act1 = new Action(mal, poran, Direction.IN, room, Case.ACCUSATIVE);
        Secondary bed = new Secondary(ObjectType.BED, Gender.FEMALE);
        ActionVerb rushedto = new ActionVerb("бросил", VerbType.REFLECTIVE);
        act1.addAction(Conjunction.AND, rushedto, Direction.ON, bed, Case.ACCUSATIVE);
        outer.println(act1.doAction());


        Actor bosse = new Actor("Боссе", Gender.MALE);
        ActionVerb rushed = new ActionVerb("кинул", VerbType.REFLECTIVE);
        Action act2 = new Action(bosse, betun, rushed, Direction.AFTER,
                                new Pronoun(Gender.MALE), Case.INSTRUMENTAL);
        outer.println(act2.doAction());

        Actor mom = new Actor("мама", Gender.FEMALE);
        ActionVerb priran = new ActionVerb("прибежал",VerbType.SIMPLE);
        Action act3 = new Action(priran, Conjunction.AND ,mom);
        outer.println(act3.doAction());

        Secondary attention = new Secondary(ObjectType.ATTENTION, Gender.NEUTRAL);
        ActionVerb directed = new ActionVerb("не обращал",VerbType.SIMPLE);
        Action act4 = new Action(Conjunction.BUT, mal, directed, Direction.ATTHEM,
                                        attention, Case.GENITIVE);
        Secondary fear = new Secondary(ObjectType.FEAR, Gender.MALE);
        ActionVerb shake = new ActionVerb("тряс",VerbType.REFLECTIVE);
        act4.addAction(Conjunction.DEF, new Pronoun(Gender.MALE), shake,
                Direction.FROM, fear, Case.GENITIVE);
        outer.println(act4.doAction());

        Actor birthday = new Actor("день рождения", Gender.MALE);
        Action act5 = new Action(birthday, Direction.SPOILED);
        outer.println(act5.doAction());

        Secondary fun = new Secondary(ObjectType.FUN);
        ActionVerb decided = new ActionVerb("решил",VerbType.SIMPLE);
        Action act6 = new Action(mal, decided, Direction.BE, fun, Case.INSTRUMENTAL);
        Secondary dog = new Secondary(ObjectType.DOG);
        act6.addAction(Conjunction.EVENIF, Direction.PRESENT, dog, Case.ACCUSATIVE);
        outer.println(act6.doAction());

        Actor get = new Actor("получить", Gender.NEUTRAL);
        Secondary puppy = new Secondary(ObjectType.PUPPY, Gender.MALE);
        Action act7 = new Action(Conjunction.BUT, get, Direction.ASPRESENT, puppy, Case.ACCUSATIVE);
        act7.addAction(" -- это уж слишком!");
        outer.println(act7.doAction());

        ActionVerb remember = new ActionVerb("вспоминал",VerbType.SIMPLE);
        Action act8 = new Action(Conjunction.WHEN,new Pronoun(Gender.MALE,Case.NOMINATIVE), Direction.ABOUT, remember);
        Actor hiscry = new Actor("его плач", Gender.MALE);
        ActionVerb turninto = new ActionVerb("преващал",VerbType.REFLECTIVE);
        Secondary moaning = new Secondary(ObjectType.MOAN, Gender.MALE);
        act8.addAction(Conjunction.COMMA, hiscry, turninto, moaning, Case.ACCUSATIVE);
        Actor he = new Actor("он", Gender.MALE);
        ActionVerb digged = new ActionVerb("зарывал",VerbType.REFLECTIVE);
        act8.addAction(Conjunction.COMMAAND, he, Direction.DEEPER, digged, Direction.HEADPILLOW);
        outer.println(act8.doAction());

        //Мама, Боссе и Бетан стояли вокруг кровати.//act9
        ActionVerb stand = new ActionVerb("стоял",VerbType.SIMPLE);
        Action act9 = new Action(mom, bosse, betun, stand, Direction.AROUND,bed,Case.GENITIVE);
        outer.println(act9.doAction());

        //Всем им было тоже очень грустно.//act10
        Actor all = new Actor("всем",Gender.PLURAL);
        Secondary wassadtoo = new Secondary (ObjectType.WST, Gender.NONE);
        Action act10 = new Action(all,new Pronoun(Gender.PLURAL,Case.DATIVE),wassadtoo,Case.DATIVE);
        outer.println(act10.doAction());
    }
}
